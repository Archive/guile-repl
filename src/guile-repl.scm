;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Ariel Rios                        ;;
;; ariel@linuxppc.org                ;;
;; http://linux.cem.itesm.mx/~ariel/ ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Released under the GPL

;; (insert str color)
;; (repl-lexer-init)
;; (repl-widget widget)
;; (repl-help func)
;; (repl-help-aux func str)
;; (repl-html-help src)
;; (repl-search-aux str rs flag)
;; (repl-search-clist-add clist str rs flag) 
;; (repl-search-select-word ls k)
;; (repl-search)
;; (repl-parenthesis-match k)
;; (repl-editor-text-cb ev)
;; (repl-editor-signal)


(define-module (grepl guile-repl)
  :use-module (ice-9 documentation)
  :use-module (gnome gnome)
  :use-module (gtk gtk)
  :use-module (gtk gdk)
  :use-module (gtk libglade)
  :use-module (gtk gtkhtml)
  :use-module (silex silex))

(load-from-path "grepl/scheme.l.scm")
(load-from-path "grepl/scheme.t.scm")

(glade-gnome-init)

(define repl:deflist #f)

(define-public repl:xml (glade-xml-new "guile-repl.glade"))

(define-public (insert str color) (gtk-text-insert grepl:editor #f color #f str -1))
(define-public (defpush) (set! repl:deflist (cons (gtk-text-get-point grepl:editor) repl:deflist)))

(define-public (repl-lexer-init)
  (set! repl:deflist '())
  (gtk-text-freeze grepl:editor)
  (lexer-init 'string (gtk-editable-get-chars grepl:editor 0 -1))
  (gtk-text-set-point grepl:editor (gtk-text-get-length grepl:editor))
  (gtk-text-backward-delete grepl:editor (gtk-text-get-length grepl:editor))
  (lexer)
  (gtk-text-thaw grepl:editor))

(define-public (repl-widget widget) (glade-xml-get-widget repl:xml widget))

(define-public (repl-help func)
  (if (equal? func "")
      (repl-help-dialog)
      (repl-help-func func)))

(define (repl-help-dialog)
  (let ((dialog (repl-widget "help-rq-dialog"))
	(button1 (repl-widget "button10"))
	(button2 (repl-widget "button11"))
	(entry   (repl-widget "combo-entry2")))
    (gtk-signal-connect button1 "clicked"
			(lambda ()  
			  (repl-help (gtk-entry-get-text entry))  
			  (gtk-button-clicked button2)))
    (gtk-signal-connect button2 "clicked"
			(lambda ()
			  (gtk-entry-set-text entry "") 
			  (gtk-widget-hide-all dialog))) 
    (gtk-widget-show-all dialog))) 

(define (repl-help-func func)
  (let ((str (object-documentation (eval-string func))))
    (if str
	(repl-help-aux func str)
	(repl-help-not-found func))))

(define (repl-help-not-found func) 
   (repl-html-help (string-append "<HTML><HEAD><TITLE>Guile Function Reference</TITLE></HEAD><BODY>\n"
				   "<H1>" func" </H1> Documentation not found </BODY></HTML>")))
(define (repl-help-aux func str)
  (let* ((k (1+ (string-index str #\))))
	 (name (string-append "<H1>" func "</H1>\n"))
	 (proto (substring str 0 k)) 
	 (rest (substring str k (string-length str))))
    (repl-html-help (string-append "<HTML><HEAD><TITLE>Guile Function Reference</TITLE></HEAD><BODY>\n"
				   name "<P><B>" proto "</B><P>" rest "</BODY></HTML>"))))

(define (repl-html-help-signal html)
   (gtk-signal-connect (repl-widget "button4") "clicked"
		       (lambda () 
			 (gtk-widget-destroy html) 
			 (gtk-widget-hide-all (repl-widget "help-dialog")))))

(define (repl-html-help src)
  (let ((html (gtk-html-new-from-string src (string-length src))))
    (gtk-container-add (repl-widget "scrolledwindow1") html)
    (gtk-widget-realize html)
    (gtk-widget-set-usize (repl-widget "scrolledwindow1") 400 400)
    (repl-html-help-signal html)
    (gtk-widget-show-all (repl-widget "help-dialog"))))

(define (repl-search-aux str rs flag)
  (let ((rx (make-regexp rs)))
    (letrec ((rx-exec 
	      (lambda (k sk ls)
		(let* ((vk (regexp-exec rx sk))
		       (lk (if vk (car (cdr (vector->list vk))) #f)))
		  (if (and lk flag)
		      (rx-exec (+ k (cdr lk)) (substring sk (cdr lk) (string-length sk))
			       (cons (vector (substring sk (car lk) (cdr lk)) 
					     (number->string (+ k (car lk))) (number->string (+ k (cdr lk)))) ls)) 
		      (reverse ls))))))
      (rx-exec 0 str '()))))

(define (repl-search-clist-add clist str rs flag) 
  (gtk-clist-clear clist)
  (let ((ls (repl-search-aux str rs flag)))
    (for-each (lambda (x) (gtk-clist-append clist x)) ls)
    ls))

(define (repl-search-select-word ls k)
  (apply (lambda (x y) (gtk-editable-select-region grepl:editor x y)) 
	 (map (lambda (x) (string->number (list-ref (vector->list (list-ref ls k)) x))) '(1 2))))

(define-public (repl-search)
  (let ((dialog (repl-widget "search-dialog"))
	(entry (repl-widget "combo-entry1"))
	(clist (repl-widget "clist1"))
	(check (repl-widget "checkbutton1"))
	(button1 (repl-widget "button5"))
	(button2 (repl-widget "button7"))
	(flag #t)
	(rlist #f))

    (gtk-signal-connect clist "select_row" (lambda (row col event)(repl-search-select-word rlist row)))
    (gtk-signal-connect check "toggled" (lambda () (set! flag (not flag))))
    (gtk-signal-connect button1 "clicked" 
			(lambda ()
			  (gtk-widget-set-sensitive clist #t) 
			  (set! rlist
				(repl-search-clist-add clist (gtk-editable-get-chars grepl:editor 0 -1) (gtk-entry-get-text entry) flag))))
    (gtk-signal-connect button2 "clicked" 
			(lambda ()
			  (gtk-clist-clear clist)
			  (gtk-entry-set-text entry "")
			  (gtk-widget-hide-all dialog)
			  (gtk-widget-set-sensitive clist #f)))

    (gtk-widget-show-all dialog)))

(define (repl-parenthesis-get-index k)
  (define (try stack str k)
    (let ((a (string-rindex str #\( 0 k))
	  (b (string-rindex str #\) 0 k)))
      (cond
       ((and a (not b) (zero? stack)) a)
       ((and a (not b)) (try (1- stack) str a))
       ((and (not a)) #f)
       ((and (> a b) (not (zero? stack))) (try (1- stack) str a))
       ((> a b) a)
       (else (try (1+ stack) str b)))))
  (try 0 (gtk-editable-get-chars grepl:editor 0 k) (1- k)))

(define (repl-parenthesis-match k)
  (let ((n (repl-parenthesis-get-index k)))
    (if n  (list grepl:editor n k) (list grepl:editor 0 0))))
 
(define (repl-editor-text-cb ev)
  (if (= (gdk-event-keyval ev) 41)
	   (call-with-dynamic-root
	    (lambda ()		  
	      (gtk-text-insert grepl:editor #f #f #f ")" -1)
	      (apply gtk-editable-select-region (repl-parenthesis-match (gtk-text-get-point grepl:editor)))
	      (gtk-signal-emit-stop-by-name grepl:editor "key_press_event"))
	    (lambda (errcode) errcode))
	   #f))

(define-public (repl-editor-signal) (gtk-signal-connect grepl:editor "key_press_event" repl-editor-text-cb))
		      


