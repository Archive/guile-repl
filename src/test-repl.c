/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

//#include <gnome.h>
#include <guile/gh.h>
//#include <guile-gtk.h>
#include <gtk/gtk.h>
#include "guile-repl.h"

static void 
load_button_cb (GtkWidget *button, gpointer data)
{
	GtkWidget *gentry = (GtkWidget *) data;
	GtkWidget *entry;
	gchar *str;
	
	entry = gnome_entry_gtk_entry (GNOME_ENTRY (gentry));
	str = gtk_entry_get_text (GTK_ENTRY (entry));
	
	gnome_entry_append_history  (GNOME_ENTRY (gentry), 0, str);
	guile_repl_load (str);
	g_free (str);
}

static void 
exec_button_cb (GtkWidget *button, gpointer data)
{
	GuileRepl *grepl = (GuileRepl *) data;

	guile_repl_exec (grepl);
}

static void
help_button_cb (GtkWidget *button, gpointer data)
{
	GtkWidget *gentry = (GtkWidget *) data;
	GtkWidget *entry;
	gchar *str;
	entry = gnome_entry_gtk_entry (GNOME_ENTRY (gentry));
	str = gtk_entry_get_text (GTK_ENTRY (entry));
	gnome_entry_append_history  (GNOME_ENTRY (gentry), 0, str);
	guile_repl_help (str);
	
}

	
static void
check_button_cb (GtkWidget *button, gpointer data)
{
	GuileRepl *grepl = (GuileRepl *) data;

	guile_repl_check_syntax (grepl);
}

static void
search_button_cb (GtkWidget *button)
{
	guile_repl_search ();
}

static void 
quit_button_cb (GtkWidget *button, gpointer data)
{ 
	gtk_main_quit ();
}

int
main (int argc, char **argv)
{
	GtkWidget *window;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *button [5];
	GtkWidget *gentry;
	GtkWidget *grepl;
	gint i = 0;
	gchar *str []= {"Load", "Exec", "Eval", "Help", "Check", "Search", "Quit"};
	scm_init_guile (); 
	gnome_init ("test-repl", "0.1", argc, argv);
	
  
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	vbox = gtk_vbox_new (FALSE, FALSE);
	hbox = gtk_hbox_new (FALSE, FALSE);		
	gentry = gnome_entry_new  ("repl");

	grepl = guile_repl_new ();
	
	for (; i < 7; i++)
		button [i] = gtk_button_new_with_label (str [i]);

	
	guile_repl_repl2scm (GUILE_REPL (grepl));
	guile_repl_editor2scm (GUILE_REPL (grepl));
	guile_repl_load_modules ();
	guile_repl_editor_signal ();
	
	gtk_window_set_title (GTK_WINDOW (window), "Guile Console");
	gtk_window_set_policy (GTK_WINDOW (window), TRUE, TRUE, FALSE);
	gtk_widget_set_usize (GTK_WIDGET (window), 500, 500);

	gtk_container_add (GTK_CONTAINER (window), vbox);
	gtk_box_pack_start (GTK_BOX (vbox), grepl, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (hbox), gentry, TRUE, TRUE, 0);
	
	for (i = 0; i < 7; i++)	
		gtk_box_pack_start (GTK_BOX (hbox), button [i], TRUE, TRUE, 0);

	gtk_signal_connect (GTK_OBJECT (button [0]), "clicked", GTK_SIGNAL_FUNC (load_button_cb), (gpointer) gentry);
	gtk_signal_connect (GTK_OBJECT (button [1]), "clicked", GTK_SIGNAL_FUNC (exec_button_cb), (gpointer) grepl);
	gtk_signal_connect (GTK_OBJECT (button [3]), "clicked", GTK_SIGNAL_FUNC (help_button_cb), (gpointer) gentry); 
	gtk_signal_connect (GTK_OBJECT (button [4]), "clicked", GTK_SIGNAL_FUNC (check_button_cb), (gpointer) grepl);
	gtk_signal_connect (GTK_OBJECT (button [5]), "clicked", GTK_SIGNAL_FUNC (search_button_cb), (gpointer) NULL);
	gtk_signal_connect (GTK_OBJECT (button [6]), "clicked", GTK_SIGNAL_FUNC (quit_button_cb), (gpointer) NULL);
	
	gtk_widget_show_all (window);
	gtk_main ();

	return 0;

}



