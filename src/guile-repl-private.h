/* -*- mode: c; c-basic-offset: 8 -*- */

/*
    This file is part of the GuileRepl library
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#ifndef GUILE_REPL_GH_H
#define GUILE_REPL_GH_H

#include <gtk/gtkwidget.h>
#include <guile/gh.h>

#ifdef _cplusplus
extern "C" {
#endif

gint listener_key_press (GtkWidget *w,  GdkEventKey *event, gpointer data);
SCM standard_handler (void *data, SCM tag, SCM throw_args); 
void repl_editor_signal (void);

#ifdef _cplusplus
}
#endif
#endif
