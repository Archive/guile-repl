/* -*- mode: c; c-basic-offset: 8 -*- */

/*
    This file is part of the GuileRepl library
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stddef.h>
#include <string.h>
#include <guile/gh.h>
//#include <guile-gtk.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>

#include "guile-repl-private.h"
#include "guile-repl.h"
									
/*
  Widget stuff
*/

static GtkBoxClass *parent_class = NULL;

static void guile_repl_class_init (GuileReplClass *klass);
static void guile_repl_init (GuileRepl *grepl);
static void guile_repl_destroy (GtkObject *object);

GtkType
guile_repl_get_type (void)
{
	static guint guile_repl_type = 0;

	if (!guile_repl_type){
		static const GtkTypeInfo guile_repl_info =
		{
			"GuileRepl",
			sizeof (GuileRepl),
			sizeof (GuileReplClass),
			(GtkClassInitFunc) guile_repl_class_init,
			(GtkObjectInitFunc) guile_repl_init,
			NULL,
			NULL,
			(GtkClassInitFunc) NULL,
		};
		guile_repl_type = gtk_type_unique (gtk_vbox_get_type (), &guile_repl_info);
	}

	return guile_repl_type;

}
	
static void
guile_repl_class_init (GuileReplClass *klass)
{
	GtkObjectClass *object_class;
	GtkWidgetClass *widget_class;
	GtkContainerClass *container_class;
	
	object_class = (GtkObjectClass *) klass;
	widget_class = (GtkWidgetClass *) klass;
	container_class = (GtkContainerClass *) klass;
	parent_class = gtk_type_class (GTK_TYPE_VBOX);

	object_class->destroy = guile_repl_destroy;
}	

static void
guile_repl_init (GuileRepl *grepl)
{
	GtkWidget *ed_swindow;
	GtkWidget *repl_swindow;
	GtkWidget *ed_frame;
	GtkWidget *repl_frame;
	
	g_return_if_fail(grepl != NULL);
	g_return_if_fail(GUILE_REPL_P (grepl));

	GTK_WIDGET_SET_FLAGS (GTK_WIDGET (grepl), GTK_NO_WINDOW);

	ed_swindow = gtk_scrolled_window_new (NULL, NULL);
	repl_swindow = gtk_scrolled_window_new (NULL, NULL);
	ed_frame = gtk_frame_new ("Editor");
	repl_frame = gtk_frame_new ("Evaluate");
	grepl->repl = gtk_text_view_new ();
	grepl->editor = gtk_text_view_new ();

	
	guile_repl_load_modules ();
	
	gtk_text_set_editable (GTK_TEXT_VIEW (grepl->repl), TRUE);
	gtk_text_set_editable (GTK_TEXT_VIEW (grepl->editor), TRUE);
	gtk_text_set_word_wrap (GTK_TEXT_VIEW (grepl->repl), FALSE);
	gtk_text_set_word_wrap (GTK_TEXT_VIEW (grepl->editor), FALSE);
	gtk_text_set_line_wrap (GTK_TEXT_VIEW (grepl->repl), FALSE);
	gtk_text_set_line_wrap (GTK_TEXT_VIEW (grepl->editor), FALSE);

	gtk_text_insert (GTK_TEXT_VIEW (grepl->repl), FALSE, FALSE, FALSE, ">", -1);

	gtk_box_pack_start (GTK_BOX (grepl), ed_frame, TRUE, TRUE, 0);
	gtk_box_pack_start (GTK_BOX (grepl), repl_frame, TRUE, TRUE, 0);

	gtk_container_add (GTK_CONTAINER (ed_swindow), grepl->editor);
	gtk_container_add (GTK_CONTAINER (repl_swindow), grepl->repl);
	gtk_container_add (GTK_CONTAINER (ed_frame), ed_swindow);
	gtk_container_add (GTK_CONTAINER (repl_frame), repl_swindow);
	gtk_signal_connect (GTK_OBJECT (grepl->repl), "key_press_event", GTK_SIGNAL_FUNC (listener_key_press), NULL);

	gtk_widget_show (grepl->repl);
	gtk_widget_show (grepl->editor);
	gtk_widget_show (ed_swindow);
	gtk_widget_show (repl_swindow);
	
}

static void
guile_repl_destroy (GtkObject *object)
{
	GuileRepl *grepl;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GUILE_REPL_P (object));

	grepl = GUILE_REPL (object);

	grepl->repl = NULL;
	grepl->editor = NULL;
	
	
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/**
 * guile_repl_new:
 * @: void
 * 
 * Construction that returns a new GuileRepl widget
 * 
 * Return value: A new #GuileRepl Widget
 **/
GtkWidget *
guile_repl_new ()
{
	GuileRepl *grepl;

	grepl = gtk_type_new (guile_repl_get_type ());

	return GTK_WIDGET (grepl);
}

/**
 * guile_repl_eval:
 * @scheme_code: Scheme code to be evaluated.
 * 
 * This function evaluates the @scheme_code using
 * GuileRepl's default handler.
 *
 * Return value: The SCM result of evaluating @scheme_code.  
 **/
SCM
guile_repl_eval (const char *scheme_code)
{
	return gh_eval_str_with_catch (scheme_code, standard_handler);
}

/*
  Hide widgets
*/

/**
 * guile_repl_hide_editor:
 * @grepl: A GuileRepl widget
 * 
 * Hides the editor text widget
 **/
void
guile_repl_hide_editor (GuileRepl *grepl)
{
	gtk_widget_hide (GTK_WIDGET (grepl->editor));
}

/**
 * guile_repl_hide_repl:
 * @grepl: A GuileRepl widget
 * 
 * Hides the repl text widget
 **/
void
guile_repl_hide_repl (GuileRepl *grepl)
{
	gtk_widget_hide (GTK_WIDGET (grepl->repl));
}

/*
  Load functions
*/

/**
 * guile_repl_load:
 * @file: Name of Scheme file to load
 * 
 * Loads file as in (load "file")
 **/
void
guile_repl_load (gchar *file)
{
	guile_repl_eval (file);
}

/**
 * guile_repl_open:
 * @grepl: A GuileRepl widget
 * @file: name of file
 * 
 * Open @file inside editor
 * 
 * Return value: 1 on success, 0 on failure
 **/
gint
guile_repl_open (GuileRepl *grepl, gchar *file)
{
	FILE *fd;
	gchar *str;
	
	if ((fd = fopen (file, "r")) == NULL)
		return (0);

	fscanf (fd, "%s", str);
	
	gtk_text_freeze (GTK_TEXT_VIEW (grepl->editor));
	gtk_text_backward_delete (GTK_TEXT_VIEW (grepl->editor), -1);
	gtk_text_insert (GTK_TEXT_VIEW (grepl->editor), FALSE, FALSE, FALSE, str, -1);
	gtk_text_thaw (GTK_TEXT_VIEW (grepl->editor));

	fclose (fd);

	return (1);
}

/**
 * guile_repl_exec:
 * @grepl: A GuileRepl widget
 * 
 * Evaluates the contents of the editor text widget.
 **/
void
guile_repl_exec (GuileRepl *grepl)
{
	guile_repl_eval (gtk_editable_get_chars (GTK_EDITABLE (grepl->editor), 0, -1));

}

/**
 * guile_repl_check_syntax:
 * @grepl: A GuileRepl widget
 * 
 * Checks scheme syntax of code inside editor text widget.
 **/
void
guile_repl_check_syntax (GuileRepl *grepl)
{
	guile_repl_eval ("(repl-lexer-init)");
}


/*
  Save functions
*/

/**
 * guile_repl_save_transcript:
 * @grepl: A GuileRepl Widget
 * @file: Name of file
 * 
 * Saves contents of repl text widget.
 * 
 * Return value: 1 on success, 0 on failure
 **/
gint
guile_repl_save_transcript (GuileRepl *grepl, gchar *file)
{
	FILE *fd;

	if ((fd = fopen (file, "w")) == NULL)
		return (0);

	fprintf (fd, "%s", guile_repl_repl_get_chars (grepl, 0, -1));

	fclose (fd);

	return (1);
}

	
/*
  Wrap up functions to avoid accesing the text widgets directly
*/
/**
 * guile_repl_editor_get_chars:
 * @grepl: A GuileRepl widget
 * @start: Starting poit
 * @end:  End point
 * 
 * Gets chars from @grepl from @start to @end
 * 
 * Return value: string from given points. 
 **/
	
gchar *
guile_repl_editor_get_chars (GuileRepl *grepl, gint start, gint end)
{
	return gtk_editable_get_chars (GTK_EDITABLE (grepl->editor), start, end);
}

/**
 * guile_repl_repl_get_chars:
 * @grepl: A GuileRepl widget
 * @start: Starting poit
 * @end:  End point
 * 
 * Gets chars from @grepl from @start to @end
 * 
 * Return value: string from given points. 
 **/
gchar *
guile_repl_repl_get_chars (GuileRepl *grepl, gint start, gint end)
{
	return gtk_editable_get_chars (GTK_EDITABLE (grepl->repl), start, end);
}

/**
 * guile_repl_insert:
 * @grepl: A GuileRepl widget
 * @str: String to be inserted
 * @color: Color of the new string
 * 
 * Inserts @str in editor text widget.
 **/
void
guile_repl_insert (GuileRepl *grepl, gchar *str, gint color)
{
	gtk_text_insert (GTK_TEXT_VIEW (grepl->editor), FALSE, FALSE, FALSE, str, -1);
}

/**
 * guile_repl_widget2scm:
 * @name: Guile name for the widget
 * @widget: A GtkWidget
 * 
 * Makes available @widget inside guile using @name
 * Please that this function enables you to redefine the
 * same name using different GtkWidget so you need to be careful.
 * Return value: 
 **/
SCM
guile_repl_widget2scm (gchar *name, GtkWidget *widget)
{
	return gh_define (name, sgtk_wrap_gtkobj (GTK_OBJECT (widget)));
}

/**
 * guile_repl_repl2scm:
 * @grepl: A GuileRepl widget
 * 
 * Makes the repl available to guile-gtk
 * 
 * Return value: SCM value of definition
 **/
SCM
guile_repl_repl2scm (GuileRepl *grepl)
{
	return guile_repl_widget2scm ("grepl:repl", grepl->repl);
}
/**
 * guile_repl_repl2scm:
 * @grepl: A GuileRepl widget
 * 
 * Makes the editor available to guile-gtk
 * 
 * Return value: SCM value of definition
 **/
SCM
guile_repl_editor2scm (GuileRepl *grepl)
{
	return guile_repl_widget2scm ("grepl:editor", grepl->editor);
}

/**
 * guile_repl_load_modules:
 * @: 
 * 
 * Loads a set of required guile modules
 * 
 * Return value: Result of module loading
 **/
SCM
guile_repl_load_modules ()
{

	return guile_repl_eval ("(use-modules (grepl guile-repl)(gnome gnome)(gtk gtk))");

}
/**
 * guile_repl_help:
 * @func: Scheme function to be searched
 * 
 * Display help for function @func
 * 
 **/
	
void
guile_repl_help (gchar *func)
{
	gchar *str = g_strconcat ("(repl-help \"", func, "\")", NULL);
	guile_repl_eval (str);
	g_free (str);
			 
}

/**
 * guile_repl_search:
 * 
 * Launch search function dialog 
 * 
 **/
void
guile_repl_search ()
{
	guile_repl_eval ("(repl-search)");

}

/**
 * guile_repl_editor_signal:
 * @: 
 * 
 * Connects matching parenthesis algorithm to
 * editor key_pressed signal. 
 **/
 
void
guile_repl_editor_signal ()
{
	guile_repl_eval ("(repl-editor-signal)");
}
