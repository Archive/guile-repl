/* -*- mode: c; c-basic-offset: 8 -*- */

/*
    This file is part of the GuileRepl library
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/


#ifndef GUILE_REPL_H
#define GUILE_REPL_H

#include <gtk/gtkwidget.h>

#ifdef _cplusplus
extern "C" {
#endif

#define GUILE_REPL(obj) GTK_CHECK_CAST (obj, guile_repl_get_type (), GuileRepl)
#define GUILE_REPL_CLASS(klass) GTK_CHECK_CLASS_CAST (klass, guile_repl_get_type (), GuileReplClass)
#define GUILE_REPL_P(obj) GTK_CHECK_TYPE (obj, guile_repl_get_type ())

struct _GuileRepl
{
	GtkVBox box;
	
	GtkWidget *repl;
	GtkWidget *editor;
};

struct _GuileReplClass
{
	GtkVBoxClass parent_class;
};

typedef struct _GuileRepl GuileRepl;
typedef struct _GuileReplClass GuileReplClass;

GtkType guile_repl_get_type (void);

GtkWidget *guile_repl_new ();

SCM guile_repl_eval (const char *scheme_code);

/*
  Hide widgets
*/

void guile_repl_hide_editor (GuileRepl *grepl);
void guile_repl_hide_repl (GuileRepl *grepl);

/*
  Load functions
*/

void guile_repl_load (gchar *file);
gint guile_repl_open (GuileRepl *grepl, gchar *file);
void guile_repl_exec (GuileRepl *grepl);
void guile_repl_check_syntax (GuileRepl *grepl);

/*
  Save functions
*/

gint guile_repl_save_transcript (GuileRepl *repl, gchar *file);

/*
  Wrap up functions to avoid accesing the text widgets directly
*/

gchar *guile_repl_editor_get_chars (GuileRepl *grepl, gint start, gint end);
#define guile_repl_editor_get_all_chars(grepl) guile_repl_editor_get_chars (grepl, 0, -1);
gchar *guile_repl_repl_get_chars (GuileRepl *grepl, gint start, gint end);
#define guile_repl_repl_get_all_chars(grepl) guile_repl_repl_get_chars (grepl, 0, -1);
void guile_repl_insert (GuileRepl *grepl, gchar *str, gint color);

SCM guile_repl_widget2scm (gchar *name, GtkWidget *widget);
SCM guile_repl_repl2scm (GuileRepl *grepl);
SCM guile_repl_editor2scm (GuileRepl *grepl);
SCM guile_repl_load_modules (void);

void guile_repl_help (gchar *func);
void guile_repl_search (void);

void guile_repl_editor_signal ();	

#ifdef _cplusplus
}
#endif
#endif


