/* -*- mode: c; c-basic-offset: 8 -*- */

/*
    This file is part of the GuileRepl library
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stddef.h>
#include <string.h>
#include <guile/gh.h>
//#include <guile-gtk.h>
#include <gtk/gtk.h>
#include <gdk/gdkkeysyms.h>
//#include <gnome.h>

#include "guile-repl-private.h"
#include "guile-repl.h"

static char *listener_prompt = ">";
static char *listener_prompt_with_cr = "\n>";

static char *
gh_print (SCM obj)
{
	char *str1;
	SCM str,val;
	SCM port;
	str = scm_makstr (0, 0);
	port = scm_mkstrport (SCM_INUM0, str, SCM_OPN | SCM_WRTNG, "scm_strprint_obj");
	scm_prin1 (obj, port, 1);
	val = scm_strport_to_string (port);
	scm_close_port (port);
	str1 = gh_scm2newstr (val,NULL);
	return (str1);
}

static void append_result (GtkWidget *repl, char *msg);

static void
eval_listener_str (GtkWidget *repl, char *buf)
{
	SCM result;
	char *str;
	
	if ((buf == NULL) || ((strlen (buf) == 1) && (buf [0] == '\n'))) 
		return;
		
	result = guile_repl_eval (buf);
	str = gh_print (result);
	append_result (repl, str);
	
	if (str)
		free(str);
}

/* check_balance stolen from scwm-0.9/utilities/scwmrepl/scwmrepl.c */

static int
check_balance (char *expr, int start, int end) 
{
	/* If you think _this_ is hairy, try doing it for C statements. */
	int i;
	int non_whitespace_p=0;
	int paren_count=0;
	int prev_separator=1;
	int quote_wait=0;

	i=start;
	while (i<end) {
		switch(expr[i]) {
		case ';' :
			/* skip till newline. */
			do {
				i++;
			} while (expr[i]!='\n' && i<end);
			break;
		case ' ':
		case '\n':
		case '\t':
		case '\r':
			if (non_whitespace_p && paren_count==0 && !quote_wait) {
				return i;
			} else {
				prev_separator=1;
				i++;
			}
			break;
		case '\"' :
			if (non_whitespace_p && paren_count==0 &&
			    !quote_wait) {
				return i;
			} else {
				/* skip past ", ignoring \" */
				do {
					i++;
					if (i < end && expr[i]=='\\') {
						i++;
					}
				} while (i < end && expr[i]!='\"');
				i++;
				if (paren_count==0) {
					if (i < end) {
						return i;
					} else {
						return 0;
					}
				} else {
					prev_separator=1;
					non_whitespace_p=1;
					quote_wait=0;
				}
			}
			break;
		case '#' :
			if (non_whitespace_p && paren_count==0 &&
			    !quote_wait) {
				return i;
			} else {
				if (prev_separator && i+1<end && expr[i+1]=='{') {
					/* skip past }#, ignoring \} */
					do {
						i++;
						if (i < end && expr[i]=='\\') {
							i++;
						}
					} while (i < end && !(expr[i]=='}' && i+1<end
							      && expr[i+1]=='#'));
					i+=2;
					if (paren_count==0) {
						if (i < end) {
							return i;
						} else {
							return 0;
						}
					} else {
						prev_separator=1;
						non_whitespace_p=1;
						quote_wait=0;
					}
					/* MS:FIXME:: Handle #\) properly! */
				} else {
					prev_separator=0;
					quote_wait=0;
					non_whitespace_p=1;
					i++;
				}
			}
			break;
		case '(' :
			if (non_whitespace_p && paren_count==0 &&!quote_wait) {
				return i;
			} else {
				i++;
				paren_count++;
				non_whitespace_p=1;
				prev_separator=1;
				quote_wait=0;
			}
			break;
		case ')' :
			paren_count--;
			if (non_whitespace_p && paren_count==0) {
				return i+1;
			} else {
				i++;
				non_whitespace_p=1;
				prev_separator=1;
				quote_wait=0;
			}
			break;
		case '\'' :
			if (prev_separator) {
				non_whitespace_p=1;
				quote_wait=1;
				prev_separator=1;
				i++;
			} else {
				non_whitespace_p=1;
				prev_separator=0;
				i++;
			}
			break;
		default :
			prev_separator=0;
			quote_wait=0;
			non_whitespace_p=1;
			i++;
			break;
		}
	}
	return 0;
}

static int last_prompt;

static void
append_listener_text (GtkWidget *repl, char *msg)
{
	int chars; /*
	chars = gtk_text_get_length(GTK_TEXT(repl));
	if (chars > 0) gtk_text_set_point(GTK_TEXT(repl),chars);
	gtk_text_insert (GTK_TEXT(repl), FALSE, FALSE, FALSE,msg,-1);

		   */
}

static void
append_result (GtkWidget *repl, char *msg)
{
	int cmd_eot;
	
	append_listener_text (repl, "\n");
	if (msg)
		append_listener_text (repl, msg);
	append_listener_text (repl, listener_prompt_with_cr);
	//cmd_eot = gtk_text_get_length (GTK_TEXT (repl));
	last_prompt = cmd_eot - 1;
}

static void
Command_Return_Callback (GtkWidget *repl)
{
	/* try to find complete form either enclosing current cursor, or just before it */
	gint new_eot=0,cmd_eot=0;
	char *str = NULL,*full_str = NULL,*prompt;
	int i,j,slen;
	int end_of_text,start_of_text,last_position,current_position,parens;
	full_str = gtk_editable_get_chars(GTK_EDITABLE(repl),0,-1);
	current_position = gtk_editable_get_position(GTK_EDITABLE(repl));
	start_of_text = current_position;
	end_of_text = current_position;
	//last_position = gtk_text_get_length(GTK_TEXT(repl));
	prompt = listener_prompt;
	if (last_position > end_of_text)
		{
			for (i=current_position;i<last_position;i++)
				if ((full_str[i+1] == prompt[0]) &&
				    (full_str[i] == '\n'))
					{
						end_of_text = i-1;
						break;
					}
		}
	if (start_of_text > 0)
		{
			for (i=current_position;i>=0;i--)
				if ((full_str[i] == prompt[0]) && 
				    ((i == 0) || 
				     (full_str[i-1] == '\n')))
					{
						start_of_text = i+1;
						break;
					}
			if (start_of_text == end_of_text)
				start_of_text = 0; /* user erased prompt? */
		}
	str = NULL;
	if (end_of_text > start_of_text)
		{
			parens = 0;
			slen = end_of_text - start_of_text + 2;
			str = (char *)calloc(slen,sizeof(char));
			for (i=start_of_text,j=0;i<=end_of_text;j++,i++) 
				{
					str[j] = full_str[i]; 
					if (str[j] == '(') parens++;
				}
			str[end_of_text-start_of_text+1] = 0;
			end_of_text = strlen(str);
			if (parens)
				{
					end_of_text = check_balance(str,0,end_of_text);
					if ((end_of_text > 0) && (end_of_text < (slen-1)))
						{
							str[end_of_text+1] = 0;
							if (str[end_of_text] == '\n') str[end_of_text]=0;
						}
					else
						{
							free(str);
							str = NULL;
							//new_eot = gtk_text_get_length(GTK_TEXT(repl));
							append_listener_text(repl, "\n");
							gtk_editable_set_position(GTK_EDITABLE(repl), 0);
							//gtk_text_get_length(GTK_TEXT(repl)));
							if (full_str) g_free(full_str);
							return;
						}
				}
			if (str)
				{
					if (current_position < (last_position-2))
						{
							append_listener_text(repl, str);
						}
					eval_listener_str (repl, str);
	
					free (str);
					str = NULL;
				}
			else
				{
					//new_eot = gtk_text_get_length(GTK_TEXT(repl));
					append_listener_text (repl, listener_prompt_with_cr);
				}
			//last_prompt = gtk_text_get_length(GTK_TEXT(repl)) - 1;
		}
	else 
		{
			//new_eot = gtk_text_get_length(GTK_TEXT(repl));
			append_listener_text(repl, "\n");
		}
	//cmd_eot = gtk_text_get_length(GTK_TEXT(repl));
	gtk_editable_set_position(GTK_EDITABLE(repl),cmd_eot); 
	if (full_str) g_free(full_str);
}

static char *C_k_str = NULL;

static void
grab_line (GtkWidget *repl)
{
	char *full_str;
	int current_position,last_position,i,j,k;
	full_str = gtk_editable_get_chars(GTK_EDITABLE(repl),0,-1);
	current_position = gtk_editable_get_position(GTK_EDITABLE(repl));
	//last_position = gtk_text_get_length(GTK_TEXT(repl));
	for (i=current_position;i<last_position;i++)
		if (full_str[i] == '\n')
			break;
	if (C_k_str) free(C_k_str);
	C_k_str = NULL;
	if (i > current_position)
		{
			C_k_str = (char *)calloc(i-current_position+2,sizeof(char));
			for (j=current_position,k=0;j<i;j++,k++) C_k_str[k] = full_str[j];
		}
	if (full_str) g_free(full_str);
}

static void
insert_line (GtkWidget *repl)
{
//	if (C_k_str)
	//	gtk_text_insert (GTK_TEXT (repl), FALSE, FALSE, FALSE ,C_k_str, strlen (C_k_str));
}

static void
back_to_start (GtkWidget *repl)
{
	char *full_str = NULL,*prompt;
	int i,start_of_text;
	full_str = gtk_editable_get_chars(GTK_EDITABLE(repl),0,-1);
	start_of_text = gtk_editable_get_position(GTK_EDITABLE(repl));
	prompt = listener_prompt;
	if (start_of_text > 0)
		{
			for (i=start_of_text;i>=0;i--)
				if ((full_str[i] == prompt[0]) && 
				    ((i == 0) || 
				     (full_str[i-1] == '\n')))
					{
						start_of_text = i+1;
						break;
					}
		}
	gtk_editable_set_position(GTK_EDITABLE(repl),start_of_text);
	if (full_str) g_free(full_str);
}

gint
listener_key_press (GtkWidget *w, GdkEventKey *event, gpointer data)
{
	if (event->keyval == GDK_Return)
		Command_Return_Callback(GTK_WIDGET (w));
	else
		{
			if (((event->keyval == GDK_k) || 
			     (event->keyval == GDK_K)) && 
			    (event->state & GDK_CONTROL_MASK))
				{
					grab_line (w);
					return (TRUE);
				}
			else
				{
					if (((event->keyval == GDK_y) || 
					     (event->keyval == GDK_Y)) && 
					    (event->state & GDK_CONTROL_MASK))
						{
							insert_line (w);
						}
					else
						{
							if (((event->keyval == GDK_a) || 
							     (event->keyval == GDK_A)) && 
							    (event->state & GDK_CONTROL_MASK))
								{
									back_to_start(w);
								}
							else
								{
									if (event->keyval == GDK_BackSpace)
										{
											int current_position;
											char *fstr;
											current_position = gtk_editable_get_position(GTK_EDITABLE(w));
											if (current_position > 1)
												{
													fstr = gtk_editable_get_chars(GTK_EDITABLE (w),
																      current_position-2,
																      current_position);
													if ((current_position != (last_prompt - 2)) && 
													    (strcmp(fstr,listener_prompt_with_cr) != 0))
														{
															g_free (fstr);
															return (TRUE);
														}
													g_free(fstr);
												}
										}
									else return(TRUE);
								}
						}
				}
		}
	gtk_signal_emit_stop_by_name (GTK_OBJECT (w),"key_press_event");
	return (TRUE);
}
void
dialog_ok_cb (GtkWidget *widget, gpointer data)
{
	GtkWidget *dialog = (GtkWidget *) data;

	gtk_widget_destroy (dialog);
}

SCM
standard_handler (void *data, SCM tag, SCM throw_args)
{
	GtkWidget *dialog;
	gchar *str = g_strconcat ("Error! Just got the following error tag: \n", SCM_CHARS (tag), NULL);

	//dialog = gnome_message_box_new (str, GNOME_MESSAGE_BOX_ERROR, GNOME_STOCK_BUTTON_OK);
	
	gtk_widget_show_all (dialog);

	g_free (str);

	return SCM_BOOL_F;

}

void
repl_editor_signal ()
{
	guile_repl_eval ("(repl-editor-signal)");
}
