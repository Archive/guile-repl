
;;    This file is part of the GuileRepl library
;;  
;;    Copyright 2001 Ariel Rios <ariel@linuxppc.org>
;;
;;    This library is free software; you can redistribute it and/or
;;    modify it under the terms of the GNU Library General Public
;;    License as published by the Free Software Foundation; either
;;    version 2 of the License, or (at your option) any later version.

;;    This library is distributed in the hope that it will be useful,
;;    but WITHOUT ANY WARRANTY; without even the implied warranty of
;;    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;    Library General Public License for more details.

;;    You should have received a copy of the GNU Library General Public License
;;    along with this library; see the file COPYING.LIB.  If not, write to
;;    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;;    Boston, MA 02111-1307, USA.

SPACE [\n\t ]

STRING \"[^"\n]*["\n]

VECTOR "\|#(\"[()0-9a-zA-Z]+\")\""

CHAR "#\\"[a-zA-Z0-9.]+ 

BOOL "#f"|"t"

NUMBER -?[0-9]+|-?[0-9]*"."[0-9]+ 

DEFINE "define"|"define-syntax"|"defmacro"

PRIMITIVES "lambda"|"quote"|"if"|"set!"|"cond"|"case"|"else"|"and"|"or"|"let"|"let*"|"letrec"|"begin"|"do"|"delay"|"quasiquote"|"not"

PREDICATES "eqv?"|"eq?"|"equal?"|[a-zA-Z0-9.]+"?"

NUMBER-PR "max"|"min"|[/*-+=]|"abs"|"quotient"|"remainder"|"modulo"|"gcd"|"lcd"|"numerator"|"denominator"|"floor"|"ceiling"|"truncate"|"round"
NUMBER-TRASC "exp"|"log"|"sin"|"cos"|"tan"|"asin"|"acos"|"atan"
NUMBER-EXP "sqrt"|"expt"
NUMBER-COORD "make-rectangular"|"make-polar"|"real-part"|"imag-part"|"magnitude"|"angle"
NUMBER-CONV "exact->inexact"|"inexact->exact"|"number->string"|"string->number"
NUMBERF {NUMBER-PR}|{NUMBER-TRASC}|{NUMBER-EXP}|{NUMBER-COORD}|{NUMBER-CONV} 

LIST "cons"|"c"[ad]+"r"|"set-car!"|"set-cdr!"|"list"|"length"|"append"|"reverse"|"list-tail"|"list-ref"|"memq"|"memv"|"member"|"assq"|"assv"|"assoc"

SYMBOL "symbol->string"|"string->symbol"

CHARF "char->integer"|"integer->char"|"char-upcase"|"char-downcase"

STRINGF "make-string"|"string"|"string-length"|"string-ref"|"string-set!"|"substring"|"string-append"|"string->list"|"list->string"|"string-copy"|"string-fill!"

VECTORF "make-vector"|"vector"|"vector-ref"|"vector-set!"|"vector->list"|"list->vector"|"vector-fill!"

CONTROLFEAT1 "apply"|"map"|"for-each"|"force"|"delay"|"make-promise"|"call-with-current-continuation"|"values"|"call-with-values"|"dynamic-wind"
CONTROLFEAT2 "eval"|"scheme-report-environment"|"null-environment"|"interaction-environment"
CONTROLFEATF {CONTROLFEAT1}|{CONTROLFEAT2}

INPUTOUTPUT1 "call-with-input-file"|"call-with-output-file"|"input-port?"|"current-input-port"|"current-output-port"
INPUTOUTPUT2 "with-input-from-file"|"with-output-to-file"|"open-input-file"|"open-output-file"|"close-input-port"|"close-output-port"
INPUTOUTPUT3 "read"|"read-char"|"peek-char"|"write"|"display"|"newline"|"write-char"|"load"|"transcript-on"|"transcript-off"
INPUTOUTPUTF {INPUTOUTPUT1}|{INPUTOUTPUT2}|{INPUTOUTPUT3}
ID [.\/a-zA-Z0-9]+ 

PARENTHESIS [()]

QUOTE "'"

QUASIQUOTE "`"

SPLICING "@"

COMMA ","

%%

{SPACE} (insert yytext "black") (yycontinue)
{STRING} (insert yytext "blue")  (yycontinue)
{CHAR} (insert yytext "blue")  (yycontinue)
{BOOL} (insert yytext "blue")  (yycontinue)
{NUMBER} (insert yytext "blue")  (yycontinue)
{DEFINE} (defpush) (insert yytext "red") (yycontinue)
{PRIMITIVES} (insert yytext "red") (yycontinue)
{PREDICATES} (insert yytext "red") (yycontinue)
{NUMBERF} (insert yytext "red" ) (yycontinue)
{LIST} (insert yytext "red") (yycontinue)
{SYMBOL} (insert yytext "red") (yycontinue)
{CHARF} (insert yytext "red") (yycontinue)
{STRINGF} (insert yytext "red") (yycontinue)
{CONTROLFEATF} (insert yytext "red") (yycontinue)
{INPUTOUTPUTF} (insert yytext "red") (yycontinue)

{ID} (insert yytext "black")  (yycontinue)
{PARENTHESIS}  (insert yytext "purple")  (yycontinue)
{QUOTE} (insert yytext "blue")  (yycontinue)
{QUASIQUOTE}  (insert yytext "blue")  (yycontinue)
{SPLICING} (insert yytext "blue")  (yycontinue)
{COMMA} (insert yytext "blue")  (yycontinue)
. (insert yytext "black")  (yycontinue)


