/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#include <gnome.h>
#include <bonobo.h>
#include <bonobo/bonobo-win.h>
#include <liboaf/liboaf.h>



#define GUILE_CTRL_OAFIID "OAFIID:bonobo_guile_control:33f7ee72-bf48-4a3d-ade2-50f06277ae63"

static void
on_destroy  (GtkWidget *app, BonoboUIContainer *uic)
{
	if (uic)
		bonobo_object_unref (BONOBO_OBJECT (uic));
	gtk_main_quit ();
		 
}

static guint
container_create (void)
{
	GtkWidget *win;
	GtkWidget *box, *control, *button;
	BonoboUIComponent *component;
	BonoboUIContainer *container;
	
	win = bonobo_window_new ("test-grepl-control", "a container for grepl");
	component = bonobo_ui_component_new ("test-grepl-control");
	container = bonobo_ui_container_new ();
	box = gtk_vbox_new (TRUE, 2);
	button = gtk_button_new_with_label ("Nothing!");

	gtk_widget_set_usize (GTK_WIDGET (win), 300, 300);
	
	bonobo_ui_container_set_win (container, BONOBO_WINDOW (win));
	bonobo_ui_component_set_container (component, bonobo_object_corba_objref (BONOBO_OBJECT (container)));
	
	control = bonobo_widget_new_control (GUILE_CTRL_OAFIID, bonobo_object_corba_objref (BONOBO_OBJECT (container)));

		if (control == NULL)
		g_error ("Cannot get `%s'.",  GUILE_CTRL_OAFIID);
	
	
	bonobo_window_set_contents (BONOBO_WINDOW (win), control);	
	gtk_box_pack_start_defaults (GTK_BOX (box), button);

	gtk_signal_connect (GTK_OBJECT (win), "destroy", on_destroy, container);

	gtk_widget_show_all (GTK_WIDGET(win));

	return FALSE;
}

int
main (int argc, char **argv)
{
	CORBA_ORB orb;

	gnome_init_with_popt_table ("test-grepl-control", "0.0", argc, argv, oaf_popt_options, 0, NULL);

	orb = oaf_init (argc, argv);

	if (!orb)
		g_error ("Initialization of orb failed!");
	if (!bonobo_init (orb, NULL, NULL))
		g_error ("could not initialize Bonobo");

	gtk_idle_add ((GtkFunction) container_create, NULL);

	bonobo_main ();

	return 0;
}
