/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#include <bonobo.h>
#include <gnome.h>
#include <guile/gh.h>
#include <liboaf/liboaf.h>

#include "control-data.h"
#include "menubar.h"
#include "toolbar.h"
#include "guile-repl.h"

#define GUILE_CTRL_FACTORY_OAFIID "OAFIID:bonobo_guile_repl_factory:dbed2440-ca6b-42a3-9410-348c1a6c2c98"

void bonobo_guile_repl_factory_init (void);

static void
set_frame_cb (BonoboControl *control, gpointer data)
{
	Bonobo_UIContainer remote_ui_container;
	BonoboUIComponent *ui_component;
	GuileReplControlData *gd = (GuileReplControlData *) data;
	GtkWidget *toolbar;
	/*
	GtkWidget *scroll_frame;
	*/
	
	remote_ui_container = bonobo_control_get_remote_ui_container (control);
	ui_component = bonobo_control_get_ui_component (control);
	bonobo_ui_component_set_container (ui_component, remote_ui_container);

	/*
	  toolbar stuff
	*/
	
	toolbar = create_toolbar (gd);
	/*
	gtk_box_pack_start (GTK_BOX (control_data->vbox), toolbar, FALSE, FALSE, 0);

	scroll_frame = e_scroll_frame_new (NULL, NULL);
	e_scroll_frame_set_shadow_type (E_SCROLL_FRAME (scroll_frame), GTK_SHADOW_IN);
	e_scroll_frame_set_policy (E_SCROLL_FRAME (scroll_frame), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
	gtk_container_add (GTK_CONTAINER (scroll_frame), GTK_WIDGET (control_data->html));
	gtk_widget_show_all (scroll_frame);
	gtk_box_pack_start (GTK_BOX (control_data->vbox), scroll_frame, TRUE, TRUE, 0);

	*/

	menubar_setup (ui_component, gd);
}
	
static BonoboObject *
bonobo_guile_repl_factory (BonoboGenericFactory *factory, void *closure)
{
	BonoboControl *control;
	GtkWidget *grepl;

	grepl = guile_repl_new ();
	gtk_widget_show (grepl);

	control = bonobo_control_new (GTK_WIDGET (grepl));
	
	guile_repl_repl2scm (GUILE_REPL (grepl));
	guile_repl_editor2scm (GUILE_REPL (grepl));
	guile_repl_load_modules ();
	guile_repl_editor_signal ();
	
	/*gtk_signal_connect (GTK_OBJECT (control), "set_frame", GTK_SIGNAL_FUNC (set_frame_cb), grepl);*/
	
	return BONOBO_OBJECT (control);
}

void 
bonobo_guile_repl_factory_init (void)
{
	static BonoboGenericFactory *guile_repl_factory = NULL;

	if (guile_repl_factory)
		return;

	guile_repl_factory = 
		bonobo_generic_factory_new (GUILE_CTRL_FACTORY_OAFIID, bonobo_guile_repl_factory, NULL);
	
	if (!guile_repl_factory)
		g_error ("I could not register the Guile Repl Control factory");

}

static void
grepl (int argc, char **argv)
{
	CORBA_ORB orb;
	
	gnome_init_with_popt_table ("bonobo_guile_repl_factory", "0.0", 
				    argc, argv, oaf_popt_options, 0, NULL);
	
	orb = oaf_init (argc, argv);
	
	if (!orb)
		g_error ("initializing orb failed");

	if (!bonobo_init (orb, NULL, NULL))
		g_error ("initializing Bonobo failed");

	bonobo_guile_repl_factory_init ();

	bonobo_main ();
}

int
main (int argc, char **argv)
{
	gh_enter (argc, argv, grepl);

	return (0);
}









