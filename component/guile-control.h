/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#ifndef GUILE_CONTROL_H
#define GUILE_CONTROL_H

#include <gtk/gtkwidget.h>

#ifdef __cplusplus 
extern "C" {
#endif


#define GUILE_CTRL(obj)  GTK_CHECK_CAST (obj, guile_cotrol_get_type (), GuileControl)
#define GUILE_CTRL_CLASS(obj)    GTK_CHECK_CLASS_CAST (klass, guile_control_get_type (), GuileControlClass)
#define GUILE_CTRL_P(obj) GTK_CHECK_TYPE (obj, guile_control_get_type())
  
typedef struct _Guile_Ctrl Guile_Ctrl;
typedef struct _Guile_Ctrl_Class Guile_Ctrl_Class;

struct _Guile_Control {
	GtkHBox hbox;
	gpointer private_data;
};

struct _Guile_Control_Class {
	GtkHBoxClass parent_class;
};
    
#ifdef __cplusplus
}
#endif 

#endif 


