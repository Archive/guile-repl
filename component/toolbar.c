/* -*- mode: c; c-basic-offset: 8 -*- */

/*
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#include <gnome.h>
#include <bonobo.h>
#include <guile/gh.h>

#include "toolbar.h"
#include "guile-repl.h"
#include "control-data.h"

static void
toolbar_help_cb (GtkWidget *button)
{
	guile_repl_help ("");
}

static void
toolbar_search_cb (GtkWidget *button)
{
	guile_repl_search ();
}

static void
toolbar_check_cb (GtkWidget *button, gpointer data)
{
	GuileRepl *grepl = (GuileRepl *) data;

        guile_repl_check_syntax (grepl);
}

GtkWidget *
create_toolbar (GuileReplControlData *gd)
{
	GtkWidget *frame;
	GtkWidget *hbox;
	GtkWidget *toolbar;
	GtkWidget *help_button;
	GtkWidget *search_button;
	GtkWidget *check_button;
	
	g_return_val_if_fail (gd->grepl != NULL, NULL);
	g_return_val_if_fail (GUILE_REPL_P (gd->grepl), NULL);

	frame = gtk_frame_new (NULL);
	hbox = gtk_hbox_new (FALSE, 0);
	toolbar = gtk_toolbar_new (GTK_ORIENTATION_HORIZONTAL, GTK_TOOLBAR_ICONS);
	help_button = gtk_button_new_with_label ("Help");
	search_button = gtk_button_new_with_label ("Search");
	check_button = gtk_button_new_with_label ("Check");
	
	gtk_container_add (GTK_CONTAINER (frame), toolbar);
	gtk_box_pack_start (GTK_BOX (hbox), frame, FALSE, FALSE, 0);
	
	gtk_toolbar_prepend_widget (GTK_TOOLBAR (toolbar), help_button, NULL, NULL);
	gtk_toolbar_prepend_widget (GTK_TOOLBAR (toolbar), search_button, NULL, NULL);
	gtk_toolbar_prepend_widget (GTK_TOOLBAR (toolbar), check_button, NULL, NULL);

	gtk_signal_connect (GTK_OBJECT (help_button), "clicked", toolbar_help_cb, NULL);
	gtk_signal_connect (GTK_OBJECT (search_button), "clicked", toolbar_search_cb, NULL);
	gtk_signal_connect (GTK_OBJECT (help_button), "clicked", toolbar_check_cb, (gpointer) gd->grepl);
	
	gtk_widget_show_all (hbox);

	return hbox;
}
