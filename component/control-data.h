/* -*- mode: c; c-basic-offset: 8 -*- */
/*
  
    Copyright 2001 Ariel Rios <ariel@arcavia.com>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License as published by the Free Software Foundation; either
    version 2 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
    Boston, MA 02111-1307, USA.

*/

#ifndef _CONTROL_DATA_H
#define _CONTROL_DATA_H

#include <bonobo.h>
#include <guile/gh.h>
#include <guile-repl.h>

typedef struct _GuileReplControlData
{
	GuileRepl *grepl;

	GtkWidget *vbox;
	GtkWidget *toolbar;
	GtkWidget *buttons [6];
} GuileReplControlData;

GuileReplControlData *guile_repl_control_data_new (GuileRepl *grepl);
void                  guile_repl_control_data_destroy (GuileReplControlData *gd);

#endif
